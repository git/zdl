#!/bin/bash -i
#
# ZigzagDownLoader (ZDL)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2011: Gianluca Zoni (zoninoz) <zoninoz@inventati.org>
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zdl
# 
# Gianluca Zoni (author)
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

## zdl-extension types: streaming
## zdl-extension name: Flix555


if [[ "$url_in" =~ flix555\. ]]
then
    html=$(curl -s \
		-A "$user_agent" \
		-c "$path_tmp"/cookies.zdl \
		"$url_in")

    input_hidden "$html"
    post_data="${post_data}&imhuman=Proceed to video"
    file_filter "$file_in"
    
    countdown- 8
    html=$(curl -s \
		-b "$path_tmp"/cookies.zdl \
		-A "$user_agent" \
		-d "${post_data}" \
		"$url_in")

    input_hidden "$html"

    if [[ "$html" =~ (Video is processing now) ]]
    then
	_log 17
	
    else
	url_in_file=$(unpack "$html")
	url_in_file="${url_in_file#*file:\"}"
	url_in_file="${url_in_file%%\"*}"
    fi
    
    end_extension
fi
