#!/bin/bash -i
#
# ZigzagDownLoader (ZDL)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2011: Gianluca Zoni (zoninoz) <zoninoz@inventati.org>
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zdl
# 
# Gianluca Zoni (author)
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

## zdl-extension types: streaming download
## zdl-extension name: Wolfstream (HD)

if [[ "$url_in" =~ wolfstream\. ]]
then
    if [[ "$url_in" =~ embed- ]]
    then
        replace_url_in "${url_in//embed-}"
    fi
    
    html=$(curl -v "$url_in")

    url_in_file=$(grep 'sources\: \[{file\:' <<< "$html")
    url_in_file="${url_in_file%\"*}"
    url_in_file="${url_in_file##*\"}"

    file_in=$(get_title "$html")
    file_in="${file_in#Watch }"

    if [[ "$url_in_file" =~ m3u8 ]]
    then
        force_dler FFMpeg
    fi
    
    end_extension
fi
