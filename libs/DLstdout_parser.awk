#
# ZigzagDownLoader (ZDL)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2011: Gianluca Zoni (zoninoz) <zoninoz@inventati.org>
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zdl
# 
# Gianluca Zoni (author)
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

function delete_notify_complete_inexistent (K, line) {
    if (exists(".zdl_tmp/notify_list.txt")) {
	while ("cat .zdl_tmp/notify_list.txt" | getline line) {
	    delete_line = "true"
	    for (K=0; K<length(percent_out); K++) {
		if ((percent_out[K] == 100) && (line == file_out[K])) {
		    delete_line = "false"
		}
	    }
	    if (delete_line == "true") {
		cmd = "sed -i -r 's|" line "||g' .zdl_tmp/notify_list.txt"
		cmd | getline
		close(cmd)
		cmd = "sed -i '/^$/d' .zdl_tmp/notify_list.txt"
		cmd | getline
		close(cmd)
	    }
	}
    }
}

function delete_tmp_complete_inexistent (K, line) {
    for (K=0; K<length(percent_out); K++) {
        if ((percent_out[K] == 100) &&
            (! exists(file_out[K]) &&
             (((url_out[K] ~ /^magnet/) || (exists(url_out[K]) && (url_out[K] ~ /\.torrent$/))) && (! is_dir(file_out[K]))) &&
             (! exists(file_out[K] ".part"))))
        {
	    #system("rm -f .zdl_tmp/"file_out[K]"_stdout.*")
            rm_file(".zdl_tmp/"file_out[K]"_stdout.*")
            
	    cmd = "sed -i -r 's|" file_out[K] "||g' .zdl_tmp/notify_list.txt"
	    cmd | getline
	    close(cmd)
	    cmd = "sed -i '/^$/d' .zdl_tmp/notify_list.txt"
	    cmd | getline
	    close(cmd)
	}
#        system("cp .zdl_tmp/" file_out[K] "_stdout.tmp .zdl_tmp/" file_out[K] "_stdout.yellow")
    }
}

function display_notify_complete (notify_list, K, line, wmname) {
    cmd = "wmctrl -m | grep Name | cut -d' ' -f2"
    cmd | getline wmname
    close(cmd)

    if (wmname != "") {
	for (K=0; K<length(percent_out); K++) {
	    if (percent_out[K] == 100) {
		if (exists(".zdl_tmp/notify_list.txt")) {
		    cmd = "grep -s \"" file_out[K] "\" .zdl_tmp/notify_list.txt"
		    cmd | getline line
		    close(cmd)
		}
		if (line != file_out[K]) {
		    notify_list = notify_list "\n" file_out[K]
		    print file_out[K] >> ".zdl_tmp/notify_list.txt"
		}
	    }
	}

	if (notify_list != "") {
	    if (wmname == "stumpwm") {
		cmd = "xprop -root -f STUMPWM_COMMAND 8s -set STUMPWM_COMMAND 'echo ^B^4*ZDL - ^B^2*File completati:^*\n" notify_list "'"
		cmd | getline line
		close(cmd)
	    }
	    else {
		cmd = "notify-send \"ZDL - File completati:\" '" notify_list "'" 
		cmd | getline line
		close(cmd)
	    }
	}
    }
}

function array_out (value, type) {
    code = code bash_array(type, i, value)
}

function downloaded_part () {
    if ((downloader_out[i] == "Axel") && (percent_out[i] < 100)) {
    	tot = int((percent_out[i] * (length_out[i] / axel_parts_out[i])) / 100)
    	return tot
    }
    else {
	return length_saved[i]
    }
}

function get_color_out () {
    if (! check_pid(pid_out[i])) {
	if (percent_out[i] == 100) {
	    color_out[i] = "green"
	}
	else if (check_irc_pid()) {
	    color_out[i] = "yellow"
	}    
	else {
	    color_out[i] = "red"
	}
    }
    else {
	if (speed_out[i] > 0) {
	    color_out[i] = "green"
	}
	else {
	    color_out[i] = "yellow"
            if (percent_out[i] == 100) {
                percent_out[i] = 0
            }
	}		    
    }
    array_out(color_out[i], "color_out")   
}

function check_stdout () {
    delete pid_alive[i]
    if (check_pid(pid_out[i])) {
	pid_alive[i] = pid_out[i]
	array_out(pid_alive[i], "pid_alive")
    }

    if ((percent_out[i] == 0) && (! check_pid(pid_out[i]))) {
	#system("rm -f .zdl_tmp/"file_out[i]"_stdout.tmp")
        rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")

        if (error_code[i] == 8) {
	    #system("rm -f " file_out[i] " " file_out[i] ".aria2")
            rm_file(file_out[i])
            rm_file(file_out[i] ".aria2")

            delete error_code[i]
	}
    }


    if ((downloader_out[i] !~ /RTMPDump|cUrl/) &&
	(pid_alive[i]) &&
	(num_check > 30)){
	code = code bash_var("num_check", "0")
	test_stdout["old"] = cat(".zdl_tmp/" file_out[i] "_stdout.old")
	if (test_stdout["new"] && test_stdout["new"] == test_stdout["old"] && 
	    ((downloader_out[i] == "Axel" &&		
             exists(file_out[i] ".st")) ||
             downloader_out[i] == "FFMpeg")) {
	    system("kill -9 " pid_out[i] " 2>/dev/null")
	}
    }

    if (check_pid(pid_out[i])) {
	if (url_in == url_out[i])
	    code = code bash_var("url_in", "")
	if (file_in == file_out[i]) {
	    code = code bash_var("file_in", "")
	    code = code bash_var("url_in", "")
	}	    
    }

    if (! check_pid(pid_out[i])) {
	if (length_saved[i] == length_out[i] &&
	    length_out[i] > 0 &&
	    ! exists(file_out[i] ".st") &&
	    ! exists(file_out[i] ".zdl") &&
	    ! downloader_out[i] ~ /Aria2|FFMpeg/)
	    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")

		
	##############################################################################################
	## cancella i file temporanei se il link non è in coda e il file non esiste
	## (potrebbe essere stato spostato o decompresso).
	## Codice lasciato commentato perché potrebbe essere un'alternativa valida (funziona).
	## Consuma qualche risorsa in più e non è un'operazione necessaria: è un lusso :)
	##############################################################################################
	# if (! exists(file_out[i])) {
	#     while (getline url_loop < ".zdl_tmp/links_loop.txt") {
	# 	if (url_loop == url_out[i]) {
	# 	    url_exists = 1
	# 	    break
	# 	}
	#     }
	#     if (url_exists) {
	# 	url_exists = 0
	#     } else {
	# 	system("rm -f .zdl_tmp/"file_out[i]"_stdout.tmp")
	#     }
	# }
		
	# check_in_file
	if (file_in == file_out[i] &&
	    url_in == url_out[i])
	    code = code bash_var("no_bis", "true")

	if (no_complete == "true") {
	    if ((exists(file_out[i]) &&
		 ! exists(file_out[i]".st") &&
		 ! exists(file_out[i]".zdl") &&
		 ! exists(file_out[i]".aria2") &&
		 length_saved[i] == length_out[i]) ||
		(downloader_out[i] == "cURL" &&
		 ! length_saved[i] &&
		 length_saved[i]>0 ) ||
		progress_end[i]) {
		#system("rm -f .zdl_tmp/" file_out[i] "_stdout.* .zdl_tmp/" file_out[i] ".MEGAenc_stdout.*")
                rm_file(".zdl_tmp/" file_out[i] "_stdout.*")
                rm_file(".zdl_tmp/" file_out[i] ".MEGAenc_stdout.*")
                
		if (exists(".zdl_tmp/notify_list.txt")) {
		    cmd = "sed -i -r 's|" file_out[i] "||g' .zdl_tmp/notify_list.txt"
		    cmd | getline
		    close(cmd)
		    cmd = "sed -i '/^$/d' .zdl_tmp/notify_list.txt"
		    cmd | getline
		    close(cmd)
		}
	    }
	}
    }
    downloaded_length = downloaded_part()
    if ((percent_out[i] == 100) ||
	(downloaded_length > 4000000)) {
	add_line(file_out[i], ".zdl_tmp/pipe_files.txt")
    }
    else if (exists(".zdl_tmp/pipe_files.txt")) {
    	rm_line(file_out[i], ".zdl_tmp/pipe_files.txt")
    }

    ## check_in_file
    if (file_in == file_out[i]) {
	code = code bash_var("length_in", length_out[i])
	code = code bash_var("length_saved_in", length_saved[i])
    }

}

function yellow_progress () {
    if (exists(".zdl_tmp/"file_out[i]"_stdout.yellow")) {
	## giallo: sostituire ciò che segue con un sistema di recupero dati precedenti (barra di colore giallo)

	c = "head -n5 .zdl_tmp/"file_out[i]"_stdout.yellow 2>/dev/null"
	nr = 0
	while (c | getline line) {
	    nr++
	    if (nr == 1) percent_out[i] = line
	    if (nr == 2) speed_out[i] = 0
	    if (nr == 3) speed_out_type[i] = line
	    if (nr == 4) eta_out[i] = line
	    if (nr == 5) length_saved[i] = line
	}
	array_out(percent_out[i], "percent_out")
	close(c)
    }
    else {
	percent_out[i] = 0
	speed_out[i] = 0
        speed_out_type[i] = "K/s"
	## mancano ancora (secondi):
	eta_out[i] = ""
	length_saved[i] = 0
    }
}

function progress_out (chunk,           progress_line, line, cmd, var_temp, array_length) {
    ## eta, %, speed, speed type, length-saved (length-out)
    if (dler == "Axel") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /(Too many redirects)/) {
	    	code = code "wget_links[" wget_links_index "]=\"" url_out[i] "\"; "
	    	break
	    } 
	    if (chunk[y] ~ /(Could not parse URL|404 Not Found)/) {
	    	progress_abort[i] = chunk[y]
	    	break
	    } 
	    if ((chunk[y] ~ "Downloaded") && (! exists(file_out[i]".st"))) {
	    	progress_end[i] = chunk[y]
	    	break
	    } 
	    if (chunk[y] ~ /%.+KB\/s.+/) {
		progress_line = chunk[y]
		split(progress_line, progress_elems, /[ ]*[%]*[K]*/)
		percent_out[i] = progress_elems[2]
		if (percent_out[i] == "]") percent_out[i] = 100
		var_temp = progress_elems[length(progress_elems)-1]
		sub(/\[/, "", var_temp)
		sub(/\..+/, "", var_temp)
		speed_out[i] = int(var_temp)
		break
	    }

	}

	if (progress_end[i]) {
	    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    if (url_in == url_out[i]) bash_var("url_in", "")
	    length_saved[i] = size_file(file_out[i])
	    percent_out[i] = 100
	    if ((file_out[i] ~ /\.MEGAenc$/) &&
	    	(!exists(file_out[i]))) {
	    	match(file_out[i], /(.+)\.MEGAenc/, matched)
		if (exists(matched[1]))
		    file_out[i] = matched[1]
	    }

	    ## conversione formato --mp3|--flac:
	    if ((format_out != "") &&
	    	(!exists(file_out[i]))) {
		match(file_out[i], /(.+)\.[^.]+$/, matched)
		if (exists(matched[1] "." format_out))
		    file_out[i] = matched[1] "." format_out
	    }
	}
	else if (progress_abort[i]) {
	    bash_var("url_in", "")
	    percent_out[i] = 0
	    code = code "_log 3 \"" url_out[i] "\"; "
	    #system("rm -f .zdl_tmp/"file_out[i]"_stdout.tmp " file_out[i] " " file_out[i] ".st " file_out[i] ".aria2")
            rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")
            rm_file(file_out[i])
            rm_file(file_out[i] ".st")
            rm_file(file_out[i] ".aria2")
	}
	else if ((speed_out[i] > 0) && (speed_out[i] ~ /^[0-9]+$/)) {
	    # speed_out_type[i] = "KB/s"
            speed_out_type[i] = "K/s"
	    ## mancano ancora:
	    if (int(speed_out[i]) != 0 && int(speed_out[i]) > 0) {
		eta_out[i] = int(((length_out[i] / 1024) * (100 - percent_out[i]) / 100) / int(speed_out[i]))
		eta_out[i] = seconds_to_human(eta_out[i])
	    }
	    length_saved[i] = int((length_out[i] * percent_out[i]) / 100)
	    if ((! no_check) && (percent_out[i] ~ /^[0-9]+$/) && (percent_out[i] > 0))
		print percent_out[i] "\n" speed_out[i] "\n" speed_out_type[i] "\n" eta_out[i] "\n" length_saved[i] > ".zdl_tmp/"file_out[i]"_stdout.yellow"
	}

    }
    else if (dler == "DCC_Xfer") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /XDCC [0-9]+ [0-9]+ [0-9]+ XDCC/) {
		split(chunk[y], progress_elems, / /)
		length_saved[i] = int(progress_elems[2])
		old_saved[i] = int(progress_elems[3])
		length_out[i] = int(progress_elems[4])
		#speed_out_type[i] = "KB/s"
                speed_out_type[i] = "K/s"
		break
	    }
	}

	if (length_saved[i] > 0 && length_saved[i] == length_out[i]) {
	    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    percent_out[i] = 100
	    speed_out[i] = 0
	    
	    if (url_in == url_out[i]) bash_var("url_in", "")	    
	}
	else if (length_out[i]) {
	    percent_out[i] = int(length_saved[i] * 100 / length_out[i])
	    speed_out[i] = (length_saved[i] - old_saved[i]) / 1024
	    
	    if (int(speed_out[i]) != 0 && int(speed_out[i]) > 0) {
		eta_out[i] = int(((length_out[i] / 1024) * (100 - percent_out[i]) / 100) / int(speed_out[i]))
		eta_out[i] = seconds_to_human(eta_out[i])
	    }

	    if ((! no_check) && (percent_out[i] ~ /^[0-9]+$/) && (percent_out[i] > 0))
		print percent_out[i] "\n" speed_out[i] "\n" speed_out_type[i] "\n" eta_out[i] "\n" length_saved[i] > ".zdl_tmp/"file_out[i]"_stdout.yellow"
	}
	else
	    yellow_progress()
    }
    else if (dler == "youtube-dl") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /\[download\]\s.+%/) {
		split(chunk[y], progress_elems, /([ ]+|\t|MiB|GiB|KiB|%|of|at|ETA)/)
		percent_out[i] = int(progress_elems[2])
		length_out[i] = int(progress_elems[6])
                #length_out[i] = 1000
		speed_out[i] = int(progress_elems[10])
                eta_out[i] = progress_elems[14]

                print "percentuale: " percent_out[i] " -- length: " length_out[i] " -- speed: " speed_out[i] " -- eta: " eta_out[i] >> "output.txt"

                for (mm=0; mm<20; mm++) {
                    print mm ": " progress_elems[mm] >> "output.txt"
                }

                if (chunk[y] ~ /KiB\/s/) {
                    speed_out_type[i] = "K/s"
                } else if (chunk[y] ~ /MiB\/s/) {
                    speed_out_type[i] = "M/s"
                } else if (chunk[y] ~ /GiB\/s/) {
                    speed_out_type[i] = "G/s"
                }
		break
	    }
	}

        if (exists(file_out[i] ".part"))
            length_saved[i] = size_file(file_out[i] ".part")
        else if (exists(file_out[i]))
            length_saved[i] = size_file(file_out[i])
        else
            length_saved[i] = 0

	if (percent_out[i] == 100) {
	    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    speed_out[i] = 0
	    
	    if (url_in == url_out[i]) bash_var("url_in", "")	    
	}
	else if (length_out[i]) {
	    #percent_out[i] = int(length_saved[i] * 100 / length_out[i])
	    #speed_out[i] = (length_saved[i] - old_saved[i]) / 1024
	    
	    if (eta_out[i]) {
                split(eta_out[i], eta_elems, /[:]/)
                if (eta_elems[3]) {
                    eta_s = eta_elems[3]
                    eta_m = eta_elems[2]
                    eta_h = eta_elems[1]
                }
                else if (eta_elems[2]) {
                    eta_s = eta_elems[2]
                    eta_m = eta_elems[1]
                    eta_h = 0
                } else if (eta_elems[1]) {
                    eta_s = eta_elems[1]
                    eta_s = 0
                    eta_s = 0
                }
                
	        eta_out[i] = seconds_to_human(eta_h * 3600 + eta_m * 60 + eta_s)
                print "eta_s: " eta_s " -- eta_m: " eta_m " -- eta_h: " eta_h " --> " eta_out[i] >> "output.txt"
	    }

            print percent_out[i] "\n" speed_out[i] "\n" speed_out_type[i] "\n" eta_out[i] "\n" length_saved[i] > ".zdl_tmp/"file_out[i]"_stdout.yellow"
	}
	else
	    yellow_progress()

    }
    else if (dler == "Aria2") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /(404 Not Found)/) {
	    	progress_abort[i] = chunk[y]
	    	break
	    }
	    if (((chunk[y] ~ "download completed") || (chunk[y] ~ "SEED")) &&
                ((! exists(file_out[i]".aria2")) || (! system("ls " file_out[i] "/*.aria2 2>/dev/null")))) {
	    	progress_end[i] = chunk[y]
	    } 
	    if (chunk[y] ~ /%/) {
		progress_line = chunk[y]

		if (! progress_end[i]) {
		    split(progress_line, progress_elems, /[(]*[%]*[:]*[K]*[\]]*/)
		    percent_out[i] = int(progress_elems[2])
		
		    match(progress_line, /DL:([0-9.]+)Mi/, matched)
		    if (matched[1]) {
			speed_out[i] = matched[1]*1024
		    }
		    else {
			match(progress_line, /DL:([0-9.]+)Ki/, matched)
			if (matched[1]) {
			    speed_out[i] = matched[1]
			}
			else {
			    match(progress_line, /DL:([0-9.]+)B/, matched)
			    speed_out[i] = matched[1]/1024
			}
		    }
		    match(progress_line, /ETA:(.+)\]/, matched)
		    eta_out[i] = matched[1]
		}
		
		match(progress_line, /\/([0-9]+)B/, matched)
		length_out[i] = matched[1]
		
		break
	    }

	}

        if (length_out[i] == 0) {
            if (is_dir( file_out[i] )) {
                length_out[i] = size_dir( file_out[i] )
            }
            else if (exists( file_out[i] )) {
                length_out[i] = size_file( file_out[i] )
            }
        }

	if (progress_end[i]) {
	    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")

	    if (url_in == url_out[i]) bash_var("url_in", "")

	    length_saved[i] = length_out[i]
	    percent_out[i] = 100

	    if ((file_out[i] ~ /\.MEGAenc$/) &&
	    	(!exists(file_out[i]))) {
	    	match(file_out[i], /(.+)\.MEGAenc/, matched)
		if (exists(matched[1]))
		    file_out[i] = matched[1]
	    }

	    ## conversione formato --mp3|--flac:
	    if ((format_out != "") &&
	    	(!exists(file_out[i]))) {
		match(file_out[i], /(.+)\.[^\.]+$/, matched)
		if (exists(matched[1] "." format_out))
		    file_out[i] = matched[1] "." format_out
	    }
	}
	else if ((url_out[i] !~ /^magnet/) && (progress_abort[i] != "")) {
	    bash_var("url_in", "")
	    percent_out[i] = 0
	    code = code "_log 3 \"" url_out[i] "\"; "
	    #system("rm -f .zdl_tmp/"file_out[i]"_stdout.tmp " file_out[i] " " file_out[i] ".aria2 " file_out[i] ".st")
            rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")
            rm_file(file_out[i])
            rm_file(file_out[i] ".st")
            rm_file(file_out[i] ".aria2")
	}
	else if ((speed_out[i] > 0) && (speed_out[i] ~ /^[0-9]+$/)) {
	    #speed_out_type[i] = "KB/s"
            speed_out_type[i] = "K/s"
	    length_saved[i] = int((length_out[i] * percent_out[i]) / 100)
	    if ((! no_check) && (percent_out[i] ~ /^[0-9]+$/) && (percent_out[i] > 0))
		print percent_out[i] "\n" speed_out[i] "\n" speed_out_type[i] "\n" eta_out[i] "\n" length_saved[i] > ".zdl_tmp/"file_out[i]"_stdout.yellow"
	}
    }
    else if (dler == "Wget") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /(saved|100%)/) {
		progress_end[i] = chunk[y]
		break
	    }
	    if (chunk[y] ~ /(404: Not Found)/) {
	    	progress_abort[i] = chunk[y]
	    	break
	    } 
	    if (chunk[y] ~ /[%]+ .+(K|M|B)/) {
		progress_line = chunk[y]
		break
	    }
	    if (chunk[y] ~ /.+K.+ ([0-9]+)[MK]+/) {
		    progress_chunked = chunk[y]
		    nolength = ""
		break
	    }
	}

	if (progress_end[i]) {
	    if (! no_check)
		rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    if (url_in == url_out[i]) bash_var("url_in", "")
	    length_saved[i] = size_file(file_out[i])
	    percent_out[i] = 100
	    if ((file_out[i] ~ /\.MEGAenc$/) &&
	    	(!exists(file_out[i]))) {
	    	match(file_out[i], /(.+)\.MEGAenc/, matched)
		if (exists(matched[1]))
		    file_out[i] = matched[1]
	    }

	    ## conversione formato --mp3|--flac:
	    if ((format_out != "") &&
	    	(!exists(file_out[i]))) {
		match(file_out[i], /(.+)\.[^\.]+$/, matched)
		if (exists(matched[1] "." format_out))
		    file_out[i] = matched[1] "." format_out
	    }
	}
	else if (progress_abort[i]) {
	    bash_var("url_in", "")
	    percent_out[i] = 0
	    code = code "_log 3 \"" url_out[i] "\"; "
	    #system("rm -f .zdl_tmp/"file_out[i]"_stdout.* " file_out[i] " " file_out[i] ".st")
            rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")
            rm_file(file_out[i])
            rm_file(file_out[i] ".st")
            rm_file(file_out[i] ".aria2")
	}
	else if (progress_line) {
	    split(progress_line, progress_elems, /[ ]*[%]*/)
	    percent_out[i] = progress_elems[length(progress_elems)-3]
	    speed_out[i] = progress_elems[length(progress_elems)-1]
	    eta_out[i] = progress_elems[length(progress_elems)]
	    if (speed_out[i] ~ /B/) speed_out_type[i]="B/s"
	    # if (speed_out[i] ~ /K/) speed_out_type[i]="KB/s"
	    # if (speed_out[i] ~ /M/) speed_out_type[i]="MB/s"
	    if (speed_out[i] ~ /K/) speed_out_type[i]="K/s"
	    if (speed_out[i] ~ /M/) speed_out_type[i]="M/s"
	    sub(/[BKM]/, "", speed_out[i])
	    length_saved[i] = size_file(file_out[i])
	    if (! length_saved[i]) length_saved[i] = 0
	}
	else if (progress_chunked) {
	    match(progress_chunked, /.+ ([0-9]+)[MK]/, matched)
	    speed_out[i] = matched[1]
	    length_saved[i] = size_file(file_out[i])
	}
	else {
	    ## giallo: sostituire ciò che segue con un sistema di recupero dati precedenti (barra di colore giallo)
	    percent_out[i] = 0
	    speed_out[i] = 0
	    #speed_out_type[i] = "KB/s"
            speed_out_type[i] = "K/s"
            ## mancano ancora (secondi):
	    eta_out[i] = ""
	    length_saved[i] = 0
	}

    }
    else if (dler == "RTMPDump") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ "Download complete") {
		progress_end[i] = chunk[y]
		break
	    }

	    if (chunk[y] ~ /\([0-9]+/) {
		progress_line = chunk[y]
		break
	    }
	}

	if (progress_end[i]) {
	    if (! no_check)
		rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    if (url_in == url_out[i]) bash_var("url_in", "")
	    length_saved[i] = size_file(file_out[i])
	    percent_out[i] = 100

	    ## conversione formato --mp3|--flac:
	    if ((format_out != "") &&
	    	(!exists(file_out[i]))) {
		match(file_out[i], /(.+)\.[^\.]+$/, matched)
		if (exists(matched[1] "." format_out))
		    file_out[i] = matched[1] "." format_out
	    }
	}
	else if (progress_line) {
	    cmd = "date +%s"
	    cmd | getline this_time
	    close(cmd)
	    elapsed_time = this_time - start_time[i]
	    split(progress_line, progress_elems, /[ ]*[%]*[\(]*/)
	    percent_out[i] = int(progress_elems[length(progress_elems)-1])
	    if (percent_out[i] > 0) {
		eta_out[i] = int((elapsed_time * 100 / percent_out[i]) - elapsed_time)
		eta_out[i] = seconds_to_human(eta_out[i])
		length_saved[i] = size_file(file_out[i])
		if (! length_saved[i]) length_saved[i] = 0
		speed_out[i] = (length_saved[i] / 1024) / elapsed_time
		#speed_out_type[i] = "KB/s"
                speed_out_type[i] = "K/s"
	    }
	    if (! pid_alive[i] && length_saved[i] < length_out[i]) {
		#system("rm -f " file_out[i])
                rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")
                rm_file(file_out[i])
                rm_file(file_out[i] ".st")
                rm_file(file_out[i] ".aria2")
            }
	}

    }
    else if (dler == "cURL") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ /[ ]+[0-9]+[k]*k$/) {
	    	progress_line = chunk[y]
	    	break
	    }
	}
        if (progress_line) {
	    split(progress_line, progress_elems, /[ ]+/)
	    speed_out[i] = progress_elems[length(progress_elems)]

	    if (speed_out[i] ~ /k$/) {
		# speed_out_type[i] = "KB/s"
                speed_out_type[i] = "K/s"
		sub(/k$/, "", speed_out[i])
	    }
	    else {
		speed_out_type[i] = "B/s"
	    }
	    
	    if (!speed_out[i])
		speed_out[i] = 0
	    
	}
	else {
	    speed_out[i] = 0
	    #speed_out_type[i] = "KB/s"
            speed_out_type[i] = "K/s"
	}
	length_saved[i] = size_file(file_out[i])

	if (!length_out[i])
	    length_out[i] = "unspecified"

    }
    else if (dler == "FFMpeg") {
	for (y=n; y>0; y--) {
            if (chunk[y] ~ /video.+muxing\soverhead/) {
                if (chunk[y-2] ~ /Error\smuxing\sa\spacket/) {
                    progress_error[i] = chunk[y]
                    break
                }
                else {
                    progress_end[i] = chunk[y]
                    break
                }
	    }
            
	    if (chunk[y] ~ /bitrate=.+speed=/) {
		progress_line = chunk[y]

		if (!speed_out[i]) {
		    match(progress_line, /bitrate=\s*(.+)kbits/, bitrate)
		    match(progress_line, /speed=\s*(.+)x/, speed)
		    speed_out[i] = int( ( bitrate[1] / 8) * speed[1] )
                    #speed_out[i] = int( ( bitrate[1] * speed[1]) / 5 )
                    speed_out_type[i] = "K/s"
		}
	    }

	    if ( chunk[y] ~ /time=/ && chunk[y] !~ /(Opening|keepalive|error)/ ) {
		progress_line = chunk[y]
				
		if (!time_out[i]) {
		    match(progress_line, /time=\s*([^ ]+)\s*/, matched)
		    time_out[i] = int( get_ffmpeg_seconds(matched[1]) )
		}
	    }

	    if (time_out[i] && speed_out[i])
		break
	}

	duration_out[i] = 0
	if (exists(".zdl_tmp/livestream_time.txt")) {
	    cmd = "cat .zdl_tmp/livestream_time.txt"
	    while (cmd | getline line) {
                split(line, matched, /\s/)
                duration_out[i] = int( get_ffmpeg_seconds(matched[3]) )
                break
	    }
	    close(cmd)
	}
	if (duration_out[i] == 0) {
	    cmd = "cat .zdl_tmp/"file_out[i]"_stdout.tmp"
	    while (cmd | getline line) {
		if (line ~ /Duration/) {
		    match(line, /Duration:\s*([^,]+)/, matched)
		    duration_out[i] = int( get_ffmpeg_seconds(matched[1]))
		    break
		}
	    }
	    close(cmd)
	}
	length_saved[i] = size_file(file_out[i])
	if (!lengh_saved[i])
	    length_saved[i] = size_file(file_out[i] ".part")

        if (length_saved[i] == 0 || length_saved[i] == "") {
            percent_out[i] = 0
        }

        if (progress_error[i] != "") {
            system("kill -9 " pid_out[i] " 2>/dev/null")
            percent_out[i] = 0
	    system("rm -f " file_out[i])
            length_out[i] = 0
            speed_out[i] = 0
            
        }
        else if ( (progress_end[i]) ||
             (time_out[i] >= duration_out[i] - 1) ||
             ( !speed_out[i] && exists(file_out[i]) && (file_out[i] !~ /\.part$/) && !check_pid(pid_out[i]) ) ) {
	    if (! no_check)
		rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
	    
	    set_awk2bash_cmd("remove_livestream_link_start " url_out[i])
	    set_awk2bash_cmd("remove_livestream_link_time " url_out[i])

	    if (url_in == url_out[i]) bash_var("url_in", "")
	    percent_out[i] = 100
        }
#        else if (progress_line != "") {
        else if (time_out[i] && duration_out[i]) {
            percent_out[i] = time_out[i] * 100 / duration_out[i]
            if (percent_out[i]) {
                length_out[i] = int(100 * length_saved[i] / percent_out[i])
            }
            else {
                length_out[i] = "unspecified"
            }
            
            if (int(speed_out[i]) != 0 && int(speed_out[i]) > 0) {
                eta_out[i] = int(((length_out[i] / 1024) * (100 - percent_out[i]) / 100) / int(speed_out[i]))
                eta_out[i] = seconds_to_human(eta_out[i])
            }
        }
        else if (! time_out[i] || ! duration_out[i]) {
            length_out[i] = "unspecified"
        }
        
        if (!speed_out[i])
            speed_out[i] = 0
        if (!length_out[i])
            length_out[i] = length_saved[i] ##"unspecified"
        # }
        # else {
        #     length_out[i] = 0
        #     length_seved[i] = 0
        #     speed_out[i] = 0
        #     percent_out[i] = 0
        # }
    }
    else if (dler == "MegaDL") {
	for (y=n; y>0; y--) {
	    if (chunk[y] ~ "^Downloaded ") {
		progress_end[i] = chunk[y]
		break
	    }
            if (chunk[y] ~ "Server returned 509") {
                speed_out[i] = 0
                delete progress_line
                system("kill -9 " pid_out[i])
                break
            }
            if (chunk[y] ~ /[%]+.+byte/) {
		progress_line = chunk[y]
		break
	    }
	}

	if (progress_end[i] &&
            (is_dir(file_out[i]) && exists(file_out[i] "/" file_out[i])) ) {
	    if (url_in == url_out[i]) bash_var("url_in", "")
	    length_saved[i] = size_dir(file_out[i])

            if ( length_saved[i] == length_out[i] ) {
                percent_out[i] = 100
            #if (is_dir(file_out[i]) && exists(file_out[i] "/" file_out[i])) {
            #     system("mv " file_out[i] " " file_out[i] "enc")
            #     system("mv " file_out[i] "enc/" file_out[i] " .")
            #     system("rm -rf " file_out[i] "enc")
            # }
            #rm_file(file_out_encoded[i])
            #    if (! no_check)
                    rm_line(url_out[i], ".zdl_tmp/links_loop.txt")
            
            }
	}
	if (progress_line) {
            length_saved[i] = size_file(file_out[i] "/" file_out_encoded[i])

            percent_out[i] = ( length_saved[i] / length_out[i] ) * 100
            
            cmd = "date +%s"
	    cmd | getline this_time
	    close(cmd)
	    elapsed_time = this_time - start_time[i]

            # # split(progress_line, progress_elems, /([:\(\)]{1}|\s\-\s|byte|%|\(|\))/)
	    # # percent_out[i] = int(progress_elems[2])
	    # split(progress_line, progress_elems, /(%)/)
            # array_length = split(progress_elems[1], progress_percent, " ")
	    # percent_out[i] = int(progress_percent[array_length])

	    if (percent_out[i] > 0) {
		eta_out[i] = int((elapsed_time * 100 / percent_out[i]) - elapsed_time)
		eta_out[i] = seconds_to_human(eta_out[i])

		if (! length_saved[i]) length_saved[i] = 0
                
                array_length = split(progress_line, speed_elems, /[()? ]/)
		speed_out[i] = speed_elems[array_length -2]
		speed_out_type[i] = speed_elems[array_length -1]

                # if (speed_out_type[i] ~ /K/) speed_out_type[i]="KB/s"
                # else if (speed_out_type[i] ~ /M/) speed_out_type[i]="MB/s"
                # else if (speed_out_type[i] ~ /B/) speed_out_type[i]="B/s"
                if (speed_out_type[i] ~ /K/) speed_out_type[i]="K/s"
                else if (speed_out_type[i] ~ /M/) speed_out_type[i]="M/s"
                else if (speed_out_type[i] ~ /B/) speed_out_type[i]="B/s"
	    }
	}        
        
	# else if (progress_line) {
	#     cmd = "date +%s"
	#     cmd | getline this_time
	#     close(cmd)
	#     elapsed_time = this_time - start_time[i]
	#     # split(progress_line, progress_elems, /([:\(\)]{1}|\s\-\s|byte|%|\(|\))/)
	#     # percent_out[i] = int(progress_elems[2])
	#     split(progress_line, progress_elems, /(%)/)
        #     array_length = split(progress_elems[1], progress_percent, " ")
	#     percent_out[i] = int(progress_percent[array_length])

	#     if (percent_out[i] > 0) {
	# 	eta_out[i] = int((elapsed_time * 100 / percent_out[i]) - elapsed_time)
	# 	eta_out[i] = seconds_to_human(eta_out[i])
	# 	length_saved[i] = size_file(file_out[i] "/" file_out_encoded[i])
	# 	if (! length_saved[i]) length_saved[i] = 0
                
        #         array_length = split(progress_line, speed_elems, /[\(\)\?]/)
	# 	speed_out[i] = speed_elems[array_length -2]
	# 	speed_out_type[i] = speed_elems[array_length -1]

        #         # if (speed_out_type[i] ~ /K/) speed_out_type[i]="KB/s"
        #         # else if (speed_out_type[i] ~ /M/) speed_out_type[i]="MB/s"
        #         # else if (speed_out_type[i] ~ /B/) speed_out_type[i]="B/s"
        #         if (speed_out_type[i] ~ /K/) speed_out_type[i]="K/s"
        #         else if (speed_out_type[i] ~ /M/) speed_out_type[i]="M/s"
        #         else if (speed_out_type[i] ~ /B/) speed_out_type[i]="B/s"
	#     }
	#     # if (! pid_alive[i] && length_saved[i] < length_out[i]) {
	#     #     #system("rm -f " file_out[i])
        #     #     rm_file(".zdl_tmp/"file_out[i]"_stdout.tmp")
        #     #     rm_file(file_out[i])
        #     #     rm_file(file_out[i] ".st")
        #     #     rm_file(file_out[i] ".aria2")
        #     # }
	# }        
    }
    
    if (! speed_out[i]) speed_out[i] = 0
    if (! speed_out_type[i]) speed_out_type[i] = "K/s" #speed_out_type[i] = "KB/s"
    if (! length_saved[i]) length_saved[i] = 0
    if (! percent_out[i]) percent_out[i] = 0

    array_out(url_out[i], "url_out")
    array_out(pid_out[i], "pid_out")

    if (pid_alive[i])
	array_out(pid_alive[i], "pid_alive")
    else
	speed_out[i] = 0
    
    array_out(downloader_out[i], "downloader_out")
    array_out(pid_prog_out[i], "pid_prog_out")
    array_out(file_out[i], "file_out")
	    
    if (downloader_out[i] ~ /DCC_Xfer|Aria2|Axel|Wget|youtube-dl|FFMpeg|MegaDL/) {
	array_out(url_out_file[i], "url_out_file")
    }
    else if (downloader_out[i] ~ /RTMPDump|cURL/) {
	array_out(streamer_out[i], "streamer_out")
	array_out(playpath_out[i], "playpath_out")
    }

    if (downloader_out[i] ~ /Aria2|Axel/) { 	
	array_out(axel_parts_out[i], "axel_parts_out")
	array_out(aria2_parts_out[i], "aria2_parts_out") 
    }

    #speed_out[i] = int(speed_out[i])
    percent_out[i] = int(percent_out[i])

    # if ((percent_out[i] == 100) && (downloader_out[i] == "MegaDL") &&
    #     is_dir(file_out[i]) && exists(file_out[i] "/" file_out[i])) {
    #     system("mv " file_out[i] " " file_out[i] "enc")
    #     system("mv " file_out[i] "enc/" file_out[i] " .")
    #     system("rm -rf " file_out[i] "enc")
    # }

    array_out(speed_out[i], "speed_out")
    array_out(speed_out_type[i], "speed_out_type")
    array_out(eta_out[i], "eta_out")
    array_out(length_saved[i], "length_saved")
    array_out(percent_out[i], "percent_out")
    array_out(length_out[i], "length_out")
        
    if (! no_check)
	check_stdout()
    
    get_color_out()
}

function get_ffmpeg_seconds (time,         h, m, s) {
    split(time, time_elems, /:/)
    h = int(time_elems[1])
    m = int(time_elems[2])
    s = int(time_elems[3])

    return int(s + (m * 60) + (h * 3600))
}

function progress () {
    ## estrae le ultime n righe e le processa con progress_out()
    delete test_stdout["new"]
    
    for (k=0;k<n;k++) {
	chunk[k] = progress_data[++j%n]
	if (! no_check)
	    test_stdout["new"] = test_stdout["new"] chunk[k]
	delete progress_data[j%n]
    }
    
    if ((num_check < 2) && (! no_check))
	print test_stdout["new"] > ".zdl_tmp/" file_out[i] "_stdout.old"
    progress_out(chunk)
    delete chunk
}

BEGIN {
    i = -1
    j = 0
    ## numero righe coda del chunk:
    n = 20
}

{
    if (FNR == 1) {
	if (j > 1) {
	    ## progress_out
	    progress()
	    j = 0
	} 
	i++
	pid_out[i] = $0

	if (check_pid(pid_out[i])) {
	    pid_alive[i] = pid_out[i]
	}
    }

    progress_data[++j%n] = $0

    if (FNR == 2) {
	if ($0 ~ /XDCC [0-9]+ [0-9]+ [0-9]+ XDCC/) {
	    system("kill -9 " pid_out[i] " 2>/dev/null")
	    system("rm -f " FILENAME)
	    i-- 
	    nextfile
	}
	else
	    url_out[i] = $0
    }
    if (FNR == 3) {
	dler = $0
	downloader_out[i] = dler
    }

    if (FNR == 4) {
	pid_prog_out[i] = $0
    }
    if (FNR == 5) {
	file_out[i] = $0
	if (dler ~ /Aria2|Axel/) yellow_progress()
    }
    if (FNR == 6) {
	if (dler ~ /DCC_Xfer|Aria2|Axel|Wget|youtube-dl|FFMpeg|MegaDL/) {
	    url_out_file[i] = $0
	}
	else if (dler ~ /RTMPDump|cURL/) {
	    streamer_out[i] = $0
	}
    }
    if (FNR == 7) {
	if (dler ~ /RTMPDump|cURL/) {
	    playpath_out[i] = $0
	}
	else if (dler ~ /Aria2|Axel/) {
	    axel_parts_out[i] = $0
	    aria2_parts_out[i] = $0
	}
        else if (dler ~ /MegaDL/) {
            file_out_encoded[i] = $0
        }
    }
    if (FNR == 8) {
        if ($0 ~ /[0-9]+/)
            start_time[i] = $0
        else
            start_time[i] = 0
    }
    if  (dler ~ /MegaDL/) {
        if (FNR == 9)
            length_out[i] = $0

        if ($0 ~ /"g":\s"/) {
            match($0, /"g":\s"(.+)"/, matched)
            if (matched[1])
                url_out_file[i] = matched[1]
        }
        #pid_out[i] = get_command_pid("megadl", "--debug api --path " file_out[i] " " url_out[i])
    }

    if ($0 ~ /Length:/ && dler == "Wget") {
	length_out[i] = $2
    }
    if ($0 ~ /File\ssize:/ && dler == "Axel") {
	length_out[i] = $3
    }
    if ($0 ~ /errorCode=8/ && dler == "Aria2") {
	error_code[i] = 8
    }
} 

END {
    progress()
    
    if (json_flag == "true") {
	for (I=0; I<length(url_out); I++) {
	    if (json_flag == "true") {
		json = json "{"
		json = json "\"path\":\"" pwd "\","
		json = json "\"link\":\"" url_out[I] "\","
		json = json "\"file\":\"" file_out[I] "\","

		if (downloader_out[I] ~ /cURL|RTMPDump/) {
		    json = json "\"playpath\":\"" playpath_out_[I] "\","
		    json = json "\"streamer\":\"" streamer_out[I] "\","
		}
		else {
		    json = json "\"url\":\"" url_out_file[I] "\","
		}
		json = json "\"downloader\":\"" downloader_out[I] "\","
		json = json "\"percent\":\"" percent_out[I] "\","
		json = json "\"eta\":\"" eta_out[I] "\","
		json = json "\"length\":\"" length_out[I] "\","
		json = json "\"saved\":\"" length_saved[I] "\","
		json = json "\"pid\":\"" pid_out[I] "\","
		json = json "\"pid_instance\":\"" pid_prog_out[I] "\","
		json = json "\"speed\":\"" speed_out[I] "\","
		json = json "\"speed_measure\":\"" speed_out_type[I] "\","
		json = json "\"max_downloads\":\"" max_dl "\","
		json = json "\"color\":\"" color_out[I] "\""

		if (I<length(file_out)-1)
		    json = json "},"
		else
		    json = json "}"
	    }
	

	    for (J=0; J<length(file_out); J++) {
		## cancella download di file con nome diverso per uno stesso link/url
		if ((url_out[I] == url_out[J]) &&
                    (url_out[I] !~ /^magnet/) &&
		    (file_out[I] != file_out[J]) &&
		    (check_pid(pid_out[I]))) {
		    #system("rm -f .zdl_tmp/"file_out[J]"_stdout.tmp " file_out[J] " " file_out[J] ".st " file_out[J] ".aria2")
                    rm_file(".zdl_tmp/"file_out[J]"_stdout.tmp")
                    rm_file(file_out[J])
                    rm_file(file_out[J] ".st")
                    rm_file(file_out[J] ".aria2")
		}
	    }
	}

        printf("%s", json) >> json_file
    }
    
    display_notify_complete()
    delete_tmp_complete_inexistent()
    delete_notify_complete_inexistent()    

    print code 
}

