#
# ZigzagDownLoader (ZDL)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2011: Gianluca Zoni (zoninoz) <zoninoz@inventati.org>
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zdl
# 
# Gianluca Zoni (author)
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

function tty () {
    c = "tty"
    c | getline t
    close(c)
    return t
}

function get_command_pid (command, args, pid) {
    gsub(/\s/, "\\0", args)
    gsub(/\//, "\\/", args)
    gsub(/\./, "\\.", args)
    gsub(/\-/, "\\-", args)
    test_string = command "\\0" args 
    print "test_string: " test_string >> "PIDAWK"
    
    c = "awk '/" test_string "/{print FILENAME}' /proc/*/cmdline"
    c | getline proc_pid_cmdline
    close(c)

    match(proc_pid_cmdline, /\/([0-9]+)\//, matched)
    pid = matched[1]
    print pid
}

function check_instance_daemon () {
    pid = $1
    c = "cat /proc/" pid "/cmdline 2>/dev/null"
    c | getline dir
    close(c)
    if (dir ~ /zdl.*silent/) {
	match(dir, /zdl.*silent\0([^\0]+)\0/, matched)

	if (matched[1] == pwd) { 
	    print pid
	    exit
	}
    }
}

function check_pid (pid,   test) {
    if (pid)
	if (exists("/proc/" pid "/cmdline")) 
	    return 1
    return 0
}

function check_irc_pid () {
    c = "grep '" url_out[i] "$' .zdl_tmp/irc-pids 2>/dev/null"
    while (c | getline line) {
	split(line, irc_pid, " ")
	if (check_pid(irc_pid[1])) return 1
    }
    close(c)
    return 0
}

function get_irc_pid () {
    c = "grep '" url_out[i] "$' .zdl_tmp/xfer-pids 2>/dev/null"
    while (c | getline pid) {
	print pid
    }
    close(c)
    return 0
}

function bash_array (name, i, value) {
    return name"["i"]=\""value"\"; "
}

function bash_var (name, value) {
    return name"=\""value"\"; "
}

function seconds_to_human (seconds,         minutes, hours) {
    minutes = int(seconds/60)
    hours = int(minutes/60)
    minutes = minutes - (hours * 60)
    seconds = seconds - (minutes * 60) - (hours * 60 * 60)
    return hours "h" minutes "m" seconds "s"
}

function cat (file,      c, line) {
    if (exists(file)) {
	chunk2 = ""
	c = "cat " file
	while (c | getline line) {
	    chunk2 = chunk2 line
	}
	close(c)
	return chunk2
    }
}

function rm_file (file) {
    if (file !~ /^[.]+$/)
        system("rm -fr " file)
}

function rm_line (line, file) {
    print "set_line_in_file - \"" line "\" \"" file "\"; " >>".zdl_tmp/awk2bash_commands"
}

function add_line (line, file) {
    print "set_line_in_file + \"" line "\" \"" file "\"; " >>".zdl_tmp/awk2bash_commands"
}

function set_awk2bash_cmd (cmd) {
    print cmd >>".zdl_tmp/awk2bash_commands"
}

function is_dir (dir) {
    return !system("test -d \"" dir "\"")
}

function exists(file,   line) {
    if (file) {
	if ((getline line < file) > 0 ) {
	    close(file)
	    return 1
	}
    }
    return 0
}

function size_file (filename) {
    ## in bytes
    c = "stat -c '%s' \"" filename "\" 2>/dev/null"
    c | getline result
    close(c)
    return result
}

function get_file_lines (filename) {
    ## in bytes
    c = "wc -l \"" filename "\" |cut -d' ' -f1"
    c | getline result
    close(c)
    return result
}

function size_dir (dir) {
    c = "du -cb \"" dir "\" 2>/dev/null | tail -n1 | cut -f1"
    c | getline result
    close(c)
    return result    
}
