#!/bin/bash -i
#
# ZigzagDownLoader (ZDL)
# 
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published 
# by the Free Software Foundation; either version 3 of the License, 
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License 
# along with this program. If not, see http://www.gnu.org/licenses/. 
# 
# Copyright (C) 2011: Gianluca Zoni (zoninoz) <zoninoz@inventati.org>
# 
# For information or to collaborate on the project:
# https://savannah.nongnu.org/projects/zdl
# 
# Gianluca Zoni (author)
# http://inventati.org/zoninoz
# zoninoz@inventati.org
#

function post_m3u8 {
    ##  *.M3U8
    if ls *__M3U8__* &>/dev/null
    then
    	list_fname=$(ls -1 "$path_tmp"/filename_*__M3U8__* 2>/dev/null    |
    			    sed -r "s|$path_tmp/filename_(.+).txt|\1|g")

    	[ -z "$list_fname" ] &&
    	    list_fname=$(ls -1 *__M3U8__*)

    	list_fprefix=(
    	    $(grep -oP '.+__M3U8__' <<< "$list_fname" |
    		     awk '!($0 in a){a[$0]; print}')
    	)

    	for fprefix in "${list_fprefix[@]}"
    	do
    	    last_seg=$(grep "$fprefix" <<< "$list_fname" | wc -l)

    	    while (( $i<=$last_seg ))
    	    do
    		unset segments
    		for ((i=1; i<=$last_seg; i++))
    		do
    		    filename=$(grep -P "${fprefix}seg-[0-9]+-" <<< "$list_fname"     |
    				      head -n1                                       |
    				      sed -r "s|(${fprefix}seg-)[^-]+(-.+)|\1$i\2|g")

    		    if [ ! -f "$filename" ] ||
    		    	   [ ! -s "$filename" ]
    		    then
    		    	_log 22
    		    	url_resume=$(grep -h "seg-${i}-" "$path_tmp"/filename_"${fprefix}"* 2>/dev/null)

    			if url "$url_resume"
    			then
    		    	    wget -qO "$filename" "$url_resume" &&
    		    		print_c 1 "$(gettext "Segment %s was recovered")" "$i" &&
    		    		break
    			else
    			    _log 24
    			    exit 1
    			fi
    		    else
    	    	    	segments[i]="$filename"
    		    fi
    		done
    	    done

    	    print_header
    	    header_box "$(gettext "%s file creation")" "${fprefix%__M3U8__}.mp4"

    	    if cat "${segments[@]}" > "${fprefix%__M3U8__}.ts" 2>/dev/null
    	    then
    		unset ffmpeg
    		command -v avconv &>/dev/null && ffmpeg="avconv"
    		command -v ffmpeg &>/dev/null && ffmpeg="ffmpeg"

    		if [ -z "$ffmpeg" ]
    		then
    		    dep=ffmpeg
    		    _log 23
		    
    		else
    		    preset=superfast # -preset [ultrafast | superfast | fast | medium | slow | veryslow | placebo]
    		    rm -f $ffmpeg-*.log

    		    if [ -e /cygdrive ]
    		    then
    			$ffmpeg -i "${fprefix%__M3U8__}.ts"       \
    				-report                           \
    				-acodec libfaac                   \
    				-ab 160k                          \
    				-vcodec libx264                   \
    				-crf 18                           \
    				-preset $preset                   \
    				-y                                \
    				"${fprefix%__M3U8__}.mp4"   &&
    			    rm -f "$fprefix"*
			
    		    else
    			( $ffmpeg -i "${fprefix%__M3U8__}.ts"       \
    				  -report                           \
    				  -acodec libfaac                   \
    				  -ab 160k                          \
    				  -vcodec libx264                   \
    				  -crf 18                           \
    				  -preset $preset                   \
    				  -y                                \
    				  "${fprefix%__M3U8__}.mp4" &>/dev/null &&
    				rm -f "$fprefix"* ) &
    			pid_ffmpeg=$!

    			ffmpeg_stdout $ffmpeg $pid_ffmpeg
    			unset key_to_continue
    		    fi
    		fi		
    	    fi
    	done
    fi
}

function post_process {
    local line format print_out
    
    ## mega.nz
    for line in *.MEGAenc
    do
	if [ -f "${path_tmp}/${line}.tmp" ] &&
	       [ ! -f "${line}.st" ] && [ ! -f "${line}.aria2" ]
	then
	    key=$(head -n1 "$path_tmp"/"$line".tmp)
	    iv=$(tail -n1 "$path_tmp"/"$line".tmp)

	    print_c 4 "$(gettext "Re-encoding of %s into %s")" "$line" "${line%.MEGAenc}"
	    openssl enc -d -aes-128-ctr -K $key -iv $iv -in "$line" -out "${line%.MEGAenc}" &&
		rm -f "${path_tmp}/${line}.tmp" "$line" &&
		print_c 1 "$(gettext "The %s file has been decrypted as %s")" "$line" "${line%.MEGAenc}"
	fi
    done

    test -f "$path_tmp"/format-post_processor &&
	read format < "$path_tmp"/format-post_processor
    
    test -f "$path_tmp"/print_out-post_processor &&
	read print_out < "$path_tmp"/print_out-post_processor

    ## --mp3/--flac: conversione formato
    if [ -n "$format" ]
    then
	command -v avconv &>/dev/null && convert2format="avconv"
	command -v ffmpeg &>/dev/null && convert2format="ffmpeg"

	data_stdout
	if [ -n "$print_out" ] && [ -s "$path_tmp"/pipe_files.txt ]
	then
	    while read line
	    do
		if ! grep -P '^$line$' $print_out &>/dev/null
		then
		    for ((i=0; i<${#pid_out[@]}; i++))
		    do
			if [ "$line" == "${file_out[i]}" ] &&
			       (( "${percent_out[i]}" == 100 ))
			then
			    echo "$line" >> "$print_out"
			fi
		    done
		fi
		
	    done < "$path_tmp"/pipe_files.txt 
	fi

	if [ -f "$print_out" ]
	then	    
	    while read line
	    do
		if [ -f "$line" ] &&
		       [ ! -f "${line}.st" ] && [ ! -f "${line}.aria2" ]
		then		    
		    mime="$(file -b --mime-type "$line")"

		    if [[ "$mime" =~ audio ]] &&
			   [[ "$line" =~ $format$ ]]
		    then
			continue
		    fi
		    
		    if [[ "$mime" =~ (audio|video) ]]
		    then
			print_header
			header_box "$(gettext "Conversion to") $format ($convert2format)"
			print_c 4 "$(gettext "File conversion"): $line"
			[ "$this_mode" == "lite" ] && convert_params="-report -loglevel quiet"

			if [ -e /cygdrive ]
			then
			    $convert2format $convert_params                   \
					    -i "$line"                        \
					    -aq 0                             \
					    -y                                \
					    "${line%.*}.$format"         &&
				rm "$line"                               &&
				print_c 1 "$(gettext "Successful conversion"): %s" "${line%.*}.$format" ||
				    print_c 3 "$(gettext "Conversion failed"): %s" "$line"
			else
			    (
				nohup $convert2format                   \
				      -i "$line"                        \
				      -report                           \
				      -aq 0                             \
				      -y                                \
				      "${line%.*}.$format" &>/dev/null     &&
				    rm "$line"                             &&
				    print_c 1 "$(gettext "Successful conversion"): %s" "${line%.*}.$format" ||
					print_c 3 "$(gettext "Conversion failed"): %s" "$line"
			    ) &
			    pid_ffmpeg=$!

			    ffmpeg_stdout $convert2format $pid_ffmpeg
			    unset key_to_continue
			fi
			print_header
		    fi
		fi
	    done < "$print_out"

	    rm "$print_out"
	fi
    fi
}
