<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'SimpleJavaScriptCompilation\\' => array($vendorDir . '/kyranrana/simple-javascript-compilation/src/main'),
    'MyCLabs\\Enum\\' => array($vendorDir . '/myclabs/php-enum/src'),
    'MatthiasMullie\\PathConverter\\' => array($vendorDir . '/matthiasmullie/path-converter/src'),
    'MatthiasMullie\\Minify\\' => array($vendorDir . '/matthiasmullie/minify/src'),
    'CloudflareBypass\\' => array($vendorDir . '/kyranrana/cloudflare-bypass/src/main'),
    'BCMathExtended\\' => array($vendorDir . '/krowinski/bcmath-extended/src/BCMathExtended'),
);
